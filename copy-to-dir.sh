#!/bin/sh
#
# Copyright (c) 2010 Mary Gardiner
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

## INFO
# This is a very simple script designed to be used when using the email client
# mutt remotely. It copies attachments to a web accessible folder and tells you
# where to view them.
#
# More details are available at http://puzzling.org/logs/thoughts/2010/May/6/mutt-attachments

# if you want to override OUTPUTDIR and VIEWINGDIR edit them here or create a
# ~/.copy-to-dir # file that looks like:
# OUTPUTDIR=someotherdir
# VIEWINGDIR=someother URI

# You can also optionally specify a location to rsync attachments to,
# RSYNCOUTPUTDIR=host:dir
# You'll probably want a passphraseless key for this and NO DELETION of rsynced
# attachments will take place.

OUTPUTDIR=$HOME/public_html/mutt
VIEWINGDIR=http://example.com/~user/mutt

CONFIGFILE=$HOME/.copy-to-dir
if [ -e "$CONFIGFILE" ]
then
	. $CONFIGFILE
fi

if [ -n "$1" ]
then
	if [ -n "$RSYNCOUTPUTDIR" ]
	then
		SHORTNAME=`basename "$1"`
		echo "Uploading attachment $SHORTNAME for viewing"
		rsync --chmod=ugo+r --progress -ptve "ssh -C" "$1" "$RSYNCOUTPUTDIR"
		echo
		echo "View attachment $SHORTNAME at $VIEWINGDIR/$SHORTNAME"
	elif [ ! -d "$OUTPUTDIR" ]
	then
		echo "ERROR: '$OUTPUTDIR' doesn't exist, or is not a directory"
	else
		cp "$1" "$OUTPUTDIR"
		SHORTNAME=`basename "$1"`
		DELETE="$OUTPUTDIR/$SHORTNAME"
		chmod 644 "$DELETE"
		echo "View attachment $SHORTNAME at $VIEWINGDIR/$SHORTNAME"
	fi
fi

# From http://www.unix.com/unix-dummies-questions-answers/5961-wait-input.html

if [ -n "RSYNCOUTPUTDIR" ]
then
	echo "Press any key to continue\c"
elif [ -n "$DELETE" ]
then
	echo "Press any key to continue, and delete viewable attachment\c"
else
	echo "No attachment specified, press any key to continue\c"
fi

oldstty=$(stty -g)
stty -icanon -echo min 1 time 0
dd bs=1 count=1 2>/dev/null
stty "$oldstty"

if [ -n "$DELETE" ]
then
	rm $DELETE
fi
